<?php

use Core\App;
use Core\Database;

$db = App::resolve(Database::class);

$currentUserId = 1;

$query = "select * from notes where id = ?";
$note = $db->query($query, [
    $_POST['id'] ,
    ])->findOrFail();

authorize(intval( $note['user_id'] ) === $currentUserId);

$db->query("delete from notes where id = ?", [$_POST['id']]);

header('location: /notes');
exit();
?>