<?php

// find the coresponding note
use Core\App;
use Core\Database;
use Core\Validator;

$db = App::resolve(Database::class);

$currentUserId = 1;

$query = "select * from notes where id = ?";
$note = $db->query($query, [
    $_POST['id'] ,
    ])->findOrFail();

// Authorize that the current user can edit the note.
authorize(intval( $note['user_id'] ) === $currentUserId);

// validate the form
$errors = [];

if (!Validator::string($_POST['body'], 1, 150) ){
    $errors['body'] = 'A Title no more than 200 charectors is required!';
}

if (!Validator::string($_POST['content'], 0, 1000) ){
    $errors['content'] = 'A Content no more than 1000 charectors is required!';
}

if (!empty($errors)){
    $tailwindClasses = include base_path('configs/tailwind-classes.php');

    return view("notes/edit.view.php", [
        'heading' => 'Edit Note' ,
        'errors' => $errors ,
        'tailwindClasses' => $tailwindClasses ,
    ]);
}

// if no validation error, update the record in the notes table.
$db->query('UPDATE notes SET body = ?, content = ? WHERE id = ?', [
    $_POST['body'] ,
    $_POST['content'] ,
    $_POST['id'] ,
]);

// redirect the user.
header('location: /notes');
die();