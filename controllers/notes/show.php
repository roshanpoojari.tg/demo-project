<?php
use Core\App;
use Core\Database;

$db = App::resolve(Database::class);
    
    $currentUserId = 1;

    $query = "select * from notes where id = ?";
    $note = $db->query($query, [
        $_GET['id'] ,
        ])->findOrFail();
    authorize(intval( $note['user_id'] ) === $currentUserId);

    $tailwindClasses = include base_path('configs/tailwind-classes.php');

    view("notes/show.view.php", [
        'heading' => $note['body'] ,
        'note' => $note ,
        'tailwindClasses' => $tailwindClasses ,
    ]);
?>