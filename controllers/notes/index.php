<?php
use Core\App;
use Core\Database;

$db = App::resolve(Database::class);

$query = "select * from notes where user_id = 1";
$notes = $db->query($query)->get();

$tailwindClasses = include base_path('configs/tailwind-classes.php');

view("notes/index.view.php", [
    'heading' => 'My Notes' ,
    'notes' => $notes ,
    'tailwindClasses' => $tailwindClasses ,
]);
?>