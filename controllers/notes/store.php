<?php 
use Core\Database;
use Core\Validator;
use Core\App;

$db = App::resolve(Database::class);

$errors = [];

if (!Validator::string($_POST['body'], 1, 150) ){
    $errors['body'] = 'A Title no more than 200 charectors is required!';
}

if (!Validator::string($_POST['content'], 0, 1000) ){
    $errors['content'] = 'A Content no more than 1000 charectors is required!';
}

if (!empty($errors)){
    $tailwindClasses = include base_path('configs/tailwind-classes.php');

    return view("notes/create.view.php", [
        'heading' => 'Add Note' ,
        'errors' => $errors ,
        'tailwindClasses' => $tailwindClasses ,
    ]);
}

$db -> query("INSERT INTO notes (body, content, user_id) VALUES (?, ?, ?);" , [
    $_POST['body'] ,
    $_POST['content'] ,
    1 ,
]);

header('location: /notes');
die();