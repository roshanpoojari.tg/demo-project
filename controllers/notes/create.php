<?php
    $tailwindClasses = include base_path('configs/tailwind-classes.php');
    
    view("notes/create.view.php", [
        'heading' => 'Add Note' ,
        'errors' => [] ,
        'tailwindClasses' => $tailwindClasses ,
    ]);