<?php require base_path("views/partials/header.php") ?>
  
<?php require base_path("views/partials/nav.php") ?>

<?php require base_path("views/partials/banner.php"); ?>

<main>
  <div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
    <form method="POST" action="/notes">
      <b>Title: </b><br>
      <input class="border-b-4 border-blue-500 bg-gray-800 w-full p-3 mb-2 rounded text-white" type="text" id="body" name="body" placeholder="Tile here" value="<?= htmlspecialchars($_POST['body']) ?? '' ?>"><br>
      
        <?php if(isset($errors['body'])) : ?>
          <p class="text-red-500 text-xs"><?= "* " .$errors['body'] ?> </p><br>
        <?php endif ?>
      <label for="content" class = "font-bold">Content:</label>
      <br>
      <textarea class="border-b-4 border-blue-500 bg-gray-800 w-full p-3 mb-2 rounded text-white" name="content" id="content" cols="30" rows="10" placeholder="Write your description here.."><?= htmlspecialchars($_POST['content']) ?? '' ?></textarea>
      <br>
        <?php if(isset($errors['content'])) : ?>
          <p class="text-red-500 text-xs"><?= "* " .$errors['content'] ?> </p><br>
        <?php endif ?>
      <br>
      <p class="text-center"><button class="<?= $tailwindClasses['buttonStyle'] ?>" type="submit">SAVE</button></p>
    </form>
    <a href="/notes" class="text-blue-400 font-bold hover:underline pl-2 pr-2">Go Back..</a>
  </div>
</main>

<?php require base_path("views/partials/footer.php") ?>