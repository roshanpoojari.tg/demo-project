<?php require base_path("views/partials/header.php") ?>
  
<?php require base_path("views/partials/nav.php") ?>

<?php require base_path("views/partials/banner.php") ?>

<main>
  <div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
    <b>Hi! This is the Notes section.</b>
    <p>Here you can add and check your notes.</p>
    <br><br>
    <b> Your notes: </b>
    <lu>
        <?php foreach ($notes as $note) : ?>
            <li>
                <a href="/note?id=<?= $note['id']?>" class="text-yellow-500 hover:underline">
                    <?=htmlspecialchars($note['body'])?>
                </a>
            </li>
        <?php endforeach ?>
    </lu>
    <p class="mt-6">
            <a href="notes/create" class="<?= $tailwindClasses['buttonStyle'] ?>">Create note</a>
    </p>
  </div>
</main>

<?php require base_path("views/partials/footer.php") ?>