<?php require base_path("views/partials/header.php") ?>
  
<?php require base_path("views/partials/nav.php") ?>

<?php require base_path("views/partials/banner.php") ?>

<main>
  <div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
    <b> Notes description: </b>
    <li><?= htmlspecialchars($note['content']);?></li>
    <br>
    <form method="POST">
      <input hidden name="_method" value="DELETE">
      <input hidden name="id" value="<?= $note['id'] ?>">
      <a href="/note/edit?id=<?= $note['id'] ?>"><button type="button" class="<?=$tailwindClasses['buttonStyle']?>">Edit</button></a>
      <button type="submit" class="<?=$tailwindClasses['redButton']?>">Delete</button>
    </form>
    <br>
    <a href="/notes" class="text-blue-400 bold hover:underline">Go Back..</a>
  </div>
</main>

<?php require base_path("views/partials/footer.php") ?>