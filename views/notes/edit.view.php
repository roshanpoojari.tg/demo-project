<?php require base_path("views/partials/header.php") ?>
  
<?php require base_path("views/partials/nav.php") ?>

<?php require base_path("views/partials/banner.php"); ?>

<main>
  <div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
    <form method="POST" action="/note">
        <input hidden name="_method" value="PATCH">
        <input hidden name="id" value="<?=$note['id']?>">
        <b>Title: </b><br>
        <input class="border-b-4 border-blue-500 bg-gray-800 w-full p-3 mb-2 rounded text-white" type="text" id="body" name="body" placeholder="Tile here" value="<?= htmlspecialchars($note['body'])?>"><br>
        
            <?php if(isset($errors['body'])) : ?>
            <p class="text-red-500 text-xs"><?= "* " .$errors['body'] ?> </p><br>
            <?php endif; ?>
        <label for="content" class = "font-bold">Content:</label>
        <br>
        <textarea class="border-b-4 border-blue-500 bg-gray-800 w-full p-3 mb-2 rounded text-white" name="content" id="content" cols="30" rows="10" placeholder="Write your description here.."><?= htmlspecialchars($note['content']) ?? '' ?></textarea>
        <br>
            <?php if(isset($errors['content'])) : ?>
            <p class="text-red-500 text-xs"><?= "* " .$errors['content'] ?> </p><br>
            <?php endif ?>
        <br>
        <p class="text-center">
        <a href="/notes"><button type="button" class="<?=$tailwindClasses['redButton']?>">CANCLE</button></a>&nbsp;&nbsp;  
        <button class="<?= $tailwindClasses['buttonStyle'] ?>" type="submit">UPDATE</button>
    </p>
    </form>
    <a href="/notes" class="text-blue-400 font-bold hover:underline pl-2 pr-2">Go Back..</a>
  </div>
</main>

<?php require base_path("views/partials/footer.php") ?>